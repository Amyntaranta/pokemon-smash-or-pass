import { writable } from "svelte/store";
import userStore from "./authedUser";
import appName from "./id";

const personalStats = writable({
  smash: 0,
  pass: 0,
});

let connection = null;

function disconnect() {
  if (connection) connection.off();
  connection = null;
}

userStore.subscribe((user) => {
  disconnect();
  connection = user.get(appName).on((stats) => {
    personalStats.set({
      smash: Object.values(stats).filter((decision) => decision === "smash")
        .length,
      pass: Object.values(stats).filter((decision) => decision === "pass")
        .length,
    });
  }, false);
});

export default personalStats;
