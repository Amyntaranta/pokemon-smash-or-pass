import user from "./user";

const username = localStorage.getItem("username");
const password = localStorage.getItem("password");

const promise = new Promise((resolve) => {
  if (username && password) {
    user.auth(username, password, () => {
      resolve();
    });
  } else {
    const username = String.random();
    const password = String.random();
    localStorage.setItem("username", username);
    localStorage.setItem("password", password);
    user.create(username, password, () => {
      user.auth(username, password, () => {
        resolve();
      });
    });
  }
});

export default {
  subscribe(subscription) {
    let cancelled = false;
    promise.then(() => {
      if (!cancelled) {
        subscription(user);
      }
    });
    return () => {
      cancelled = true;
    };
  },
};
