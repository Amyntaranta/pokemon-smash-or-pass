import db from "./db";
import { writable } from "svelte/store";
import appName from "./id";

const stats = writable({});

db.get("#" + appName)
  .map()
  .once((pubkey) => {
    db.get(pubkey)
      .get(appName)
      .on((data) => {
        if (!(data instanceof Object)) return;
        stats.update((statsNow) => ({ ...statsNow, [pubkey]: data }));
      });
  });

function view(stats) {
  // copilot wrote this and i don't know what it does (it works)
  const result = {};
  Object.keys(stats).forEach((pubkey) => {
    Object.keys(stats[pubkey]).forEach((key) => {
      if (!result[key]) result[key] = {};
      if (!result[key][stats[pubkey][key]]) result[key][stats[pubkey][key]] = 0;
      result[key][stats[pubkey][key]]++;
    });
  });
  return result;
}

export default {
  subscribe(subscription) {
    return stats.subscribe((s) => subscription(view(s)));
  },
};
