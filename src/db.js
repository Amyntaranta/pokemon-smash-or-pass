import Gun from "gun/gun";

import "gun/sea";
import "gun/lib/not";

const db = new Gun("https://gun-us.herokuapp.com/gun");
window.db = db;

Gun.chain.subscribe = function (subscription) {
  let connection = null;
  let enabled = true;
  this.once((data, key) => {
    if (!enabled) return;
    subscription(data, key);
    connection = this.on((data, key) => {
      if (enabled) subscription(data, key);
    });
  });
  return () => {
    enabled = false;
    if (connection && !this.memleak) connection.off();
  };
};

export default db;
